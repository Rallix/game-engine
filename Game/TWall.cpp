#include "TWall.h"

namespace Game::Breakout
{
	TWall::TWall(std::pair<float, float> radius, float height, const int bricks, const int rows, int specialBrick)
	: Radius(std::move(radius)), Rows(rows), BricksCount(bricks), SpecialBrick(specialBrick), Height(height)
	{

		if (specialBrick % 2 == 0) SpecialBrick = std::clamp(specialBrick - 1, 1, bricks);
		Reconstruct();
	}

	void TWall::Reconstruct()
	{
		Bricks.clear();
		//! Build a castle
		for (int row = 0; row < Rows; row++)
		{
			std::vector<TBrick> brickRow;
			for (int b = 1; b <= BricksCount; b++)
			{				
				const int durability = std::clamp(Rows - row - 1, 1, 3);
				auto& brick = brickRow.emplace_back(Radius, Height, (360.0f/3.0f) / float(BricksCount), row, durability);
				if (row == Rows-1 && b % 2 == 0) brick.SetActive(false); // crenelations
			}
			Bricks.push_back(brickRow);
			// Placement
			for (int b = 1; b <= BricksCount; b++)
			for (int col = b; col < BricksCount; col++) {
				Bricks[row][col].Turn(360.0f / float(BricksCount));
			}		
		}
		
		if (SpecialBrick >= 0 && SpecialBrick <= BricksCount)
		{
			// random between crenelations
			auto& special = Bricks[Rows - 2][SpecialBrick];
			special.IsSpecial = true;
			special.Durability = 1;
			special.RefreshColor();			
		}
	}


	void TWall::Break(const int column, const bool completely)
	{		
		auto& brick = Bricks[0][column];
		
		brick.Durability = std::clamp(brick.Durability - 1, 0, brick.Durability);
		if (completely) brick.Durability = 0;
		if (brick.Durability == 0)
		{
			// Fall down
			for (int row = 1; row < Rows; row++)
			{				
				auto& topBrick = Bricks[row][column];
				auto& brickBelow = Bricks[row - 1][column];
				
				if (!topBrick.IsActive)
				{
					brickBelow.SetActive(false);
					break;
				}
				
				if (row == Rows - 1) topBrick.SetActive(false);
				
				brickBelow.Durability = topBrick.Durability;
				brickBelow.IsSpecial = topBrick.IsSpecial;
				brickBelow.RefreshColor();
			}
		}
		else
		{
			brick.RefreshColor();
		}
		
	}

	bool TWall::CheckVictory() const
	{
		for (const auto brick : Bricks[0])
		{
			if (brick.IsActive) return false;
		}
		return true;
	}

	void TWall::Render() const
	{		
		for (auto& row : Bricks)
		{
			for (auto& brick : row) if (brick.IsActive) {
				brick.Render();
			}
		}
	}
}
