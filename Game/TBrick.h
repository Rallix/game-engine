#pragma once

#include <vector>
#include "../Engine/TVector.h"
#include "../Engine/TMatrix.h"
#include "../Engine/Mathf.cpp"

#include "IDrawable.h"
#include "Properties.h"

namespace Game::Breakout
{
	/** A brick which can be destroyed by a ball. */
	class TBrick final : IDrawable // : public TPaddle // can inherit because it looks and behaves the same (but gets destroyed in the process)
	{
	private:
		float Color[4] = { 1,1,1,1 }; // handled by 'RefreshColor'
		bool TurnsRight = true;

		/** Recalculates normals etc. Use every time Points are updated. */
		inline void RefreshInfo()
		{
			RefreshColor();
			UpdateNormals();
		}

		void UpdateNormals();
	public:
		std::pair<float, float> Radius;
		float Height{}; // The height of the brick
		std::vector<Engine::TVector> Points;
		std::vector<Engine::TVector> Normals;
		int Durability = 1;
		int Row = 1;

		bool IsActive{}; // Is the brick enabled (visible, interactable) or not?		
		bool IsSpecial = false; //  Is the brick special (grants powerup) or not?

		// A small value determining how far the paddles move with arrow keys.
		inline static const float StepAngle = 2;

		/** Constructs a (fixed shape) paddle at a given position on a circle with specified inner and outer radius. */
		explicit TBrick(std::pair<float, float> radius, float height, float circularAngle, int row = 1, int durability = 1);

		/** Activates or deactivates (hides) a brick. */
		inline void SetActive(bool active) { IsActive = active; };

		/** Rotates the paddle in the right (+) or left (-) direction. */
		void Turn(const float angle);

		/** Applies a color based on its durability and special status */
		void RefreshColor();

		void Render() const override;
	};

}