#pragma once

//! Debug
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <windows.h>
#include <string>
#include <stdexcept>

#include "../Breakout/GL/freeglut.h"
#include "GameInfo.h"

namespace Game::Breakout
{
	//! Game Info
	inline GameInfo Game = GameInfo();	

	void NewGame();

	using namespace std::string_literals;
	/** Menu which is displayed when the player clicks the right mouse button. */
	inline void RightClickMenu(const int option)
	{
		switch (option)
		{
		default:
			throw std::invalid_argument("Unhandled right click menu option: '"s + std::to_string(option) + "'"s);
		case 1:
			//! New game
			NewGame();
			return;
		case 2:
			//! Quit
			exit(0);
		}
	}


}
