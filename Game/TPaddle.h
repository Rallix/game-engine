#pragma once

#include <vector>
#include "../Engine/TVector.h"
#include "../Engine/TMatrix.h"
#include "../Engine/TMatrix.cpp"

#include "IDrawable.h"

namespace Game::Breakout
{
	/** Circular paddles controlled by the player used to deflect the balls. */
	class TPaddle : IDrawable
	{
	private:
		float Color[4] = {1,1,1,1};
		
		

		bool turnsRight = true;

		/** Recalculates normals etc. Use every time Points are updated. */
		inline void RefreshInfo()
		{
			UpdateNormals();
		}
		void UpdateNormals();
		
	public:
		std::pair<float, float> Radius;
		float Height;

		std::vector<Engine::TVector> Points;
		std::vector<Engine::TVector> Normals;
		
		// A small value determining how far the paddles move with arrow keys.
		inline static const float StepAngle = 2;
		
		/** Constructs a (fixed shape) paddle at a given position on a circle with specified inner and outer radius. */
		explicit TPaddle(const float color[4], std::pair<float, float> radius, float height, float circularAngle);

		/** Rotates the paddle in the right (+) or left (-) direction. */
		void Turn(const float angle);

		void Render() const override;
	};
}