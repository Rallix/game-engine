#pragma once
#include "../Engine/TVector.h"
#include "../Engine/TVector.cpp"
#include "IDrawable.h"

namespace Game::Breakout
{
	using namespace Engine;
	
	/** A circular ground marking the game area. */
	class TGround final : IDrawable
	{		
	public:
		/** The center point of the game arena. */
		const TVector Center;
		
		/** The radius of the game arena. */
		const float Radius;
		
		/** All the boundary points of the game arena. */
		vector<TVector> Points;

		/** The perpendicular direction of the object's surface. */
		const TVector Normal = TVector::Up();
		
		/** S,T,R texture coordinates. */
		vector<TVector> TexPoints;
		
		#pragma region Constructors & Operators
		/** Creates a circular ground with given radius. */
		explicit TGround(const float radius): TGround(radius, size_t(32)) {}

		/** Creates a circular ground with given radius, and number of points. */
		explicit TGround(float radius, size_t points);
		
		#pragma endregion Constructors & Operators

		void Render() const override;
		
	};
}
