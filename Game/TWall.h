#pragma once
#include <utility>
#include "TBrick.h"
#include "IDrawable.h"


namespace Game::Breakout
{
	/** A circular wall made of breakable bricks. */
	class TWall : IDrawable
	{
		public:
			std::vector<std::vector<TBrick>> Bricks; // Rows of bricks
			std::pair<float, float> Radius;
			int Rows;
			int BricksCount;
			int SpecialBrick;
			float Height;
		
			TWall(std::pair<float, float> radius, float height, int bricks, int rows, int specialBrick = -1);

			/** Restores all bricks to their initial value. */
			void Reconstruct();			

			/** A brick being hit by a ball. */
			void Break(int column, bool completely = false);

			/** Checks if all bricks in the bottom row are inactive, thus determining victory. */
			bool CheckVictory() const;

			void Render() const override;
	};
}
