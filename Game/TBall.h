#pragma once
#include "IDrawable.h"
#include "../Engine/TRay.h"
#include "../Engine/TRay.cpp"


namespace Game::Breakout
{
	using namespace Engine;
	
	/** A ball bouncing between paddles, destroying bricks. */
	class TBall final : public IDrawable
	{
	public:
		explicit TBall() : TBall(2) {}
		explicit TBall(const float radius) : Radius(radius) {};

		/* The radius of the ball. */
		const float Radius;
		// TVector Direction;
		/* The velocity of the ball. */
		TVector Velocity = TVector::Zeros(); // <- speed * direction
		/* The position of the ball. */
		TVector Position = TVector::Zeros();
		// TVector Rotation // a bit pointless with a sphere
		// TVector Scale // see radius

		bool IsSpecial = false; // Power-up, causing the ball to break through durable blocks
		inline static const float MinSpeed = 0.9f; // 1.00f;
		inline static const float MaxSpeed = 1.15f; // 1.20f;
		float Speed = MinSpeed;

		/** Renders a sphere onscreen. */
		void Render() const override;

		/** Moves the ball forward. */
		void Translate(float time);

		/** Moves the ball forward. */
		inline void Translate() { Translate(Speed); }
		
		/** Changes the velocity of the ball. */
		void AddForce(TVector& vector, float speed);

		/** Stops the ball from moving and resets its speed. */
		inline void Stop()
		{
			auto noDirection = TVector::Zeros();
			AddForce(noDirection, MinSpeed);
		}

		/** Bounces the ball away. */
		// void Bounce(TVector direction); // unnecessary -- 'Vector.Reflect' does all the work

		bool IsOutOfBounds(float radius) const;

		bool IsTouchingPaddle(std::pair<float, float> radius, std::vector<TVector> paddle, float randomness);		
		
	};
}
