#include "TBall.h"
#include "../Engine/Mathf.cpp"

namespace Game::Breakout
{
	void TBall::Render() const
	{
		glPushAttrib(GL_CURRENT_COLOR);
		float gold[4] = {1, 0.85f, 0, 1};
		if (IsSpecial) glColor3fv(gold);
		glPushMatrix();
		glTranslated(Position[0], Position[1] + double(Radius), Position[2]);
		glutSolidSphere(Radius, 16, 16); // 16 UV Sphere
		glPopMatrix();
		glPopAttrib();
	}

	void TBall::Translate(float time)
	{
		const auto distance = time * Velocity;
		Position += distance;
	}

	void TBall::AddForce(TVector& vector, const float speed)
	{
		const auto flattenedVelocity = TVector(vector[0], 0, vector[2]);
		Velocity = flattenedVelocity.Normalized() * speed;
	}

	bool TBall::IsOutOfBounds(const float radius) const
	{		
		const auto ballExtent = Position.Magnitude() + Radius;
		return radius <= ballExtent; // inside bounds

		// Bounce
		/*
		if (radius > ballExtent) return false;
		const auto normal = (TVector() - Position).Normalized();
		Velocity = Velocity.Reflect(normal);
		return true;
		*/
	}

	bool TBall::IsTouchingPaddle(std::pair<float, float> radius, std::vector<TVector> paddle, float randomness = 0)
	{
		const auto ballMagnitude = Position.Magnitude();
		const auto ballExtends = std::make_pair(ballMagnitude + Radius, ballMagnitude - Radius);
		if (radius.first > ballExtends.first || radius.second < ballExtends.second) return false; // inside area || outside area
		
		const auto center = TVector::Zeros();
		const auto fromCenter = center.Towards(Position).Normalized();
		//! Basic bounce
		const auto leftmostPoint = center.Towards(paddle[7]); // Leftmost point of the paddle
		const auto rightmostPoint = center.Towards(paddle[4]); // Rightmost point of the paddle
		//! Sides
		const auto direction = Velocity.Normalized();

		//! Corners

		TVector normal;
		if (fromCenter.IsBetween(leftmostPoint, rightmostPoint))
		{			
			//! 'Back' bounces shouldn't be too important, because it would bounce towards the outer edge anyway
			normal = fromCenter;
		}
		else if (direction.IsBetween(Position.Towards(paddle[7]), Position.Towards(paddle[0])))
		{
			normal = paddle[6].Towards(paddle[7]).Normalized();
		}
		else if (direction.IsBetween(Position.Towards(paddle[3]), Position.Towards(paddle[4])))
		{
			normal = paddle[2].Towards(paddle[3]).Normalized();
		}
		else return false;

		const float adjust = 1 - randomness; // adjust the direction slightly to make it more 'enjoyable'
		const auto random = Mathf::RandomDirection(-1, 1);
		const auto newDirection = Velocity.Reflect(normal).Normalized();				
		Velocity = newDirection * Speed;

		int max = 200;
		while (center.Towards(Position).IsBetween(leftmostPoint, rightmostPoint) 
			&& center.Distance(Position) + Radius > radius.first && --max > 0) {
			Position += newDirection * 0.05f;
		}
		Velocity = newDirection.Interpolate(random, adjust).Interpolate(fromCenter.Inverted(), 0.75f).Normalized() * Speed;
		return true;
	}
}
