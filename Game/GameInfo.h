﻿#pragma once
#include <ctime>

namespace Game::Breakout
{
	enum class GameState { Ready, Playing, Paused, Won, Lost };
	enum class CameraMode { Perspective, Top, Ball };
	
	/** Contains information about the current game. */
	struct GameInfo
	{
		explicit GameInfo()
		{
			State = GameState::Ready;
		}
	
		void StartGame(const int lives = 5)
		{
			Lives = lives > 0 ? lives : 1; // clamp [1, Inf)
			State = GameState::Paused;
			StartTime = time(nullptr);			
		}

		/** Returns the time elapsed from starting the game. */
		[[nodiscard]] int GetElapsedTime() const
		{
			
			const time_t currentTime = (State == GameState::Won || State == GameState::Lost) ? FinalTime : time(nullptr);			
			return -static_cast<int>(difftime(StartTime, currentTime));
		}

		/** Concludes the game. */
		void EndGame(const bool victory)
		{
			State = victory ? GameState::Won : GameState::Lost;
			FinalTime = time(nullptr);
		}

		int Lives = 5;
		GameState State = GameState::Ready;
		CameraMode Camera = CameraMode::Perspective;

	private:
		time_t StartTime = time(nullptr);
		time_t FinalTime = time(nullptr);
		
	};
}
