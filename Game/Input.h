#pragma once
#include "Menu.h"

namespace Game::Breakout
{	

	/** Contains infromation about pressed keys. */
	struct InputInfo
	{
		// Arrow keys
		bool Left = false;
		bool Right = false;		

		bool Space = false;

		int Number = -1;
		
		void Reset()
		{
			*this = {}; // all goes to their defaults | disallows multiple keypresses
		}
	};

	inline InputInfo Input;
	
	/** Triggered every time a key is pressed. Marks input keys as "pressed". */
	inline void GetKeyDown(unsigned char key, int x, int y)
	{
		if (Game.State == GameState::Ready) return; // only allow keyboard input while actually playing

		switch (key)
		{
			case 'A':
			case 'a':
				Input.Left = true;
				break;
			case 'D':
			case 'd':
				Input.Right = true;
				break;
			case VK_SPACE:
				Input.Space = true;
				break;
			case '1':
				Input.Number = 1;
				break;
			case '2':
				Input.Number = 2;
				break;
			case '3':
				Input.Number = 3;
				break;
			default:
				break;
		}
	}
	
	/** Triggered every time a key is released. Unmarks input keys from being pressed. */
	inline void GetKeyUp(unsigned char key, int x, int y)
	{
		// Input.Reset();		
		if (Game.State == GameState::Ready) return; // only allow keyboard input while actually playing

		switch (key)
		{
			case 'A':
			case 'a':
				Input.Left = false;
				break;
			case 'D':
			case 'd':
				Input.Right = false;
				break;
			case VK_SPACE:
				Input.Space = false;
				break;
			case '1':
				[[fallthrough]];
			case '2':
				[[fallthrough]];
			case '3':
				Input.Number = -1;
				break;
			default:
				break;
		}
	}
}
