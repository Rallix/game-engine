#include <cstdlib>
#include <stdexcept>
#include <string>

#include "Menu.h"

namespace Game::Breakout
{
	using namespace std::string_literals;

	/** Menu which is displayed when the player clicks the right mouse button. */
	void RightClickMenu(const int option)
	{
		switch (option)
		{
		default:
			throw std::invalid_argument("Unhandled right click menu option: '"s + std::to_string(option) + "'"s);
		case 1:
			//! New game
			// TODO: Start a new game
			return;
		case 2:
			//! Quit
			exit(0);
		}
	}
}