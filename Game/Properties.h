#pragma once

namespace Game::Breakout
{

	/** Static properties which don't change (much) throughout the game. */

	//! Resolution
	inline int Width = 1280;//1024;
	inline int Height = 720;//768;

	//! Colors
	inline float White[4] = { 1, 1, 1, 1 };
	inline float Black[4] = { 0, 0, 0, 1 };	
	inline float LightGrey[4] = { 0.75f, 0.75f, 0.75f, 1 };
	inline float Grey[4] = { 0.5f, 0.5f, 0.5f, 1 };
	inline float DarkGrey[4] = { 0.35f, 0.35f, 0.35f, 1 };
	
	inline float Red[4] = { 1, 0, 0, 1 };
	inline float Green[4] = { 0, 1, 0, 1 };
	inline float Blue[4] = { 0, 0, 1, 1 };
	inline float Cyan[4] = { 0, 1, 1, 1 };
	
	inline float Brown[4] = { 0.5f, 0.35f, 0.05f, 1 };
	inline float Purple[4] = { 0.3333f, 0, 1, 1 };
	
	inline float BackgroundColor[4] = { 41.0f / 255, 79.0f / 255, 122.0f / 255, 1.0f };

	//! Input
	inline int Mouse[2] = {0, 0}; // initial coordinates of the mouse pointer
}
