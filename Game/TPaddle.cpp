#include "TPaddle.h"

#include <utility>

namespace Game::Breakout
{
	using namespace Engine;
	
	TPaddle::TPaddle(const float color[4], std::pair<float, float> radius, const float height, const float circularAngle)
		: Radius(std::move(radius)), Height(height)
	{
		for (int c = 0; c <= 3; c++) Color[c] = color[c];
		
		const auto base = TVector::Right();
		const auto elevation = TVector::Up() * height;
		const TMatrix rotate = TMatrix::Rotation(TVector::Up(), circularAngle);

		//! Points (8 + 8 above) -- fixed shape
		const int points = 16; // do not change
		Points.resize(points);
		Points[0] = base * Radius.first; // inner ring
		for (int p = 1; p <= 3; p++) Points[p] = rotate * Points[p-1];		
		Points[7] = base * Radius.second; // outer ring
		for (int p = 6; p >= 4; p--) Points[p] = rotate * Points[p+1];
		for (int p = 8; p < points; p++) Points[p] = Points[p-8] + elevation; // "extrude"
		RefreshInfo();
	}

	void TPaddle::Turn(const float angle)
	{
		if (Mathf::Approximately(0, angle)) return; // pointless rotation		
		turnsRight = angle > 0;
		const TMatrix rotate = TMatrix::Rotation(TVector::Up(), angle);
		// for (auto& point : Points) point = rotate * point;
		for (size_t p = 0; p < Points.size(); p++) 
		{
			Points[p] = rotate * Points[p];
		}
		RefreshInfo();
	}

	void TPaddle::Render() const
	{
		glPushAttrib(GL_CURRENT_COLOR);
		glColor3fv(Color);
		
		//! Inner ring
		glBegin(GL_QUAD_STRIP); // "Draws a connected group of quadrilaterals."

		glNormal3d(Normals[0][0], Normals[0][1], Normals[0][2]);
		glVertex3d(Points[0][0], Points[0][1], Points[0][2]);
		glVertex3d(Points[8][0], Points[8][1], Points[8][2]);
		glVertex3d(Points[1][0], Points[1][1], Points[1][2]);
		glVertex3d(Points[9][0], Points[9][1], Points[9][2]);
		glNormal3d(Normals[1][0], Normals[1][1], Normals[1][2]);
		glVertex3d(Points[2][0], Points[2][1], Points[2][2]);
		glVertex3d(Points[10][0], Points[10][1], Points[10][2]);
		glNormal3d(Normals[2][0], Normals[2][1], Normals[2][2]);
		glVertex3d(Points[3][0], Points[3][1], Points[3][2]);
		glVertex3d(Points[11][0], Points[11][1], Points[11][2]);
		
		glNormal3d(Normals[3][0], Normals[3][1], Normals[3][2]);
		glVertex3d(Points[4][0], Points[4][1], Points[4][2]);
		glVertex3d(Points[12][0], Points[12][1], Points[12][2]);

		//! Outer ring
		glNormal3d(Normals[4][0], Normals[4][1], Normals[4][2]);
		glVertex3d(Points[5][0], Points[5][1], Points[5][2]);
		glVertex3d(Points[13][0], Points[13][1], Points[13][2]);
		glNormal3d(Normals[5][0], Normals[5][1], Normals[5][2]);
		glVertex3d(Points[6][0], Points[6][1], Points[6][2]);
		glVertex3d(Points[14][0], Points[14][1], Points[14][2]);
		glNormal3d(Normals[6][0], Normals[6][1], Normals[6][2]);
		glVertex3d(Points[7][0], Points[7][1], Points[7][2]);
		glVertex3d(Points[15][0], Points[15][1], Points[15][2]);
		
		glNormal3d(Normals[7][0], Normals[7][1], Normals[7][2]);
		glVertex3d(Points[0][0], Points[0][1], Points[0][2]);
		glVertex3d(Points[8][0], Points[8][1], Points[8][2]);

		glEnd();

		//! Top
		glBegin(GL_QUAD_STRIP); // "Draws a connected group of quadrilaterals."

		glNormal3d(Normals[8][0], Normals[8][1], Normals[8][2]);
		glVertex3d(Points[15][0], Points[15][1], Points[15][2]);
		glVertex3d(Points[8][0], Points[8][1], Points[8][2]);
		glVertex3d(Points[14][0], Points[14][1], Points[14][2]);
		glVertex3d(Points[9][0], Points[9][1], Points[9][2]);
		glVertex3d(Points[13][0], Points[13][1], Points[13][2]);
		glVertex3d(Points[10][0], Points[10][1], Points[10][2]);
		glVertex3d(Points[12][0], Points[12][1], Points[12][2]);
		glVertex3d(Points[11][0], Points[11][1], Points[11][2]);

		glEnd();
		glPopAttrib();
	}

	void TPaddle::UpdateNormals()
	{
		Normals.clear();
		Normals.resize(9);
		for (size_t p = 0; p < 7; p++)
		{
			const auto topDown = Points[p+8] - Points[p];
			const auto leftRight = Points[p+1] - Points[p];
			Normals[p] = topDown.Cross(leftRight).Normalized();
		}
		Normals[7] = (Points[15] - Points[7]).Cross(Points[0] - Points[7]).Normalized();
		Normals[8] = (Points[15] - Points[8]).Cross(Points[9] - Points[8]).Normalized();
	}

	
}
