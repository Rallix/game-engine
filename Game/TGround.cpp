#include "TGround.h"
#include "../Engine/Mathf.cpp"

namespace Game::Breakout {	

	TGround::TGround(const float radius, const size_t points) : Center(0,0,0), Radius(radius)
	{
		// Position
		Points = Mathf::GetCircleCoords(Radius, points);
		Points.push_back(Points[0]); //! back to begginning
		if (Points.size() != points + 1) throw std::invalid_argument("A circle with incorrect amount of points was created.");
		// Texture
		for (auto& p : Points) TexPoints.push_back(p.STR(radius));
		if (TexPoints.size() != points + 1) throw std::invalid_argument("Incorrect amount of texture coordinates were created.");
	}

	void TGround::Render() const
	{
		glNormal3d(Normal[0], Normal[1], Normal[2]);
		glBegin(GL_TRIANGLE_FAN);
		glTexCoord3f(0.5f, 0.5f, 0);
		glVertex3d(0, 0, 0);
		for (size_t i = 0; i < Points.size(); i++)
		{
			glTexCoord3d(TexPoints[i][0], TexPoints[i][1], TexPoints[i][2]);
			glVertex3d(Points[i][0], Points[i][1], Points[i][2]);
		}
		glEnd();
	}
}
