#pragma once
#include <string>

#include "../Breakout/GL/freeglut.h"

namespace Game::Breakout
{
	/** A set of objects which can (should) be displayed onscreen. */
	class IDrawable
	{
	public:
		 
		/** Renders an object onscreen. */
		virtual void Render() const;
		inline virtual ~IDrawable() = default;

		/** Displays a text onscreen. */
		static void ShowText(const GLdouble x, const GLdouble y, const std::string &string, const float* color, void* font = GLUT_BITMAP_HELVETICA_18)
		{
			glColor3d(color[0], color[1], color[2]);
			glRasterPos2d(x, y);
			for (char n : string) glutBitmapCharacter(font, n);
			glColor3d(1, 1, 1); // default to white
		}
		/** Displays a white text onscreen. */
		static void ShowText(const GLdouble x, const GLdouble y, const std::string &string, void* font = GLUT_BITMAP_HELVETICA_18)
		{
			const float white[] = {1,1,1,1};
			ShowText(x, y, string, white, font);
		}
		
	};
	/** Renders an object onscreen. */
	inline void IDrawable::Render() const {}
}
