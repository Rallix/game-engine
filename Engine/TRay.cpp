﻿#include "TRay.h"
#include "../Engine/Mathf.cpp"

namespace Game::Engine
{
	//? Ray = infinite line (currently), or a limited segment?

	float TRay::Distance(const TRay& ray) const
	{
		const auto n = this->Direction.Cross(ray.Direction);
		const auto line = this->Origin.Towards(ray.Origin);
		if (!Mathf::Approximately(n.Magnitude(), 0.0f))
		{
			return abs(n.Dot(line) / n.Magnitude());
		}
		else
		{
			return this->Direction.Cross(line).Magnitude() / this->Direction.Magnitude();
		}
	}

	float TRay::Distance(const TVector& point) const
	{
		const auto towardsPoint = this->Origin.Towards(point);
		return this->Direction.Cross(towardsPoint).Magnitude() / this->Direction.Magnitude();
	}

	TVector TRay::GetPoint(const float distance) const
	{
		const auto direction = this->Direction.Normalized();
		return this->Origin + direction * distance;
	}
}


