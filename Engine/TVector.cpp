#pragma once
#include <cmath>

#include "TVector.h"
#include "../Engine/Utils.cpp"
#include "../Engine/Mathf.cpp"

namespace Game::Engine
{
#pragma region Constructors
	TVector::TVector(const float x, const float y)
	{
		Data.clear();
		Data.push_back(x);
		Data.push_back(y);
	}

	TVector::TVector() : TVector({0, 0, 0}) {}

	TVector::TVector(const float x, const float y, const float z) : TVector(x, y)
	{
		Data.push_back(z);
	}

	TVector::TVector(const float x, const float y, const float z, const float w) : TVector(x, y, z)
	{
		Data.push_back(w);
	}

#pragma endregion Constructors

#pragma region Operators

	TVector& TVector::operator +=(const TVector& other)
	{
		// compound assignment (does not need to be a member but often is, to modify the private members)

		/* addition of other to *this takes place here */
		if (!AreSameSize(*this, other)) throw std::invalid_argument("The vectors being added aren't of same size!");
		for (int i = 0; i < this->GetLength(); i++)
		{
			this->Data[i] += other.Data[i];
		}

		return *this; // return the result by reference
	}	

	TVector& TVector::operator *=(const float& scalar)
	{
		for (int i = 0; i < this->GetLength(); i++)
		{
			this->Data[i] *= scalar;
		}
		return *this;
	}

	TVector& TVector::operator*=(const TVector& other)
	{
		if (!AreSameSize(*this, other))
			throw std::
				invalid_argument("The vectors being multiplied aren't of same size!");
		for (int i = 0; i < this->GetLength(); i++)
		{
			this->Data[i] *= other.Data[i];
		}
		return *this;
	}

	bool TVector::operator ==(const TVector& other) const
	{
		if (this->GetLength() != other.GetLength()) return false;
		for (int i = 0; i < GetLength(); ++i)
		{
			if (!Mathf::Approximately(this->Get(i), other.Get(i))) return false;			
		}
		return true;
	}

#pragma endregion Operators


	bool TVector::AreSameSize(const TVector& a, const TVector& b)
	{
		return a.GetLength() == b.GetLength();
	}

	std::string TVector::ToString() const
	{
		std::string s;
		const int length = GetLength();
		switch (length)
		{
		case 0:
		case 1:
			s = "V_(invalid)";
			break;
		default:
			s = Format("V%d(", length);
			for (int i = 0; i < length; ++i)
			{
				s.append(Format("%.3f", Get(i)));
				if (i < length - 1) s.append(", ");
			}
			s.append(")");
			break;
		}
		return s;
	}

#pragma region Math
	float TVector::Dot(const TVector& other) const
	{
		if (!AreSameSize(*this, other)) throw std::invalid_argument("The vectors don't have the same size!");

		float dot = 0.0f;
		for (int i = 0; i < GetLength(); ++i)
		{
			dot += this->Get(i) * other.Get(i);
		}
		return dot;
	}

	TVector TVector::Cross(const TVector& other) const
	{
		const int length = this->GetLength();
		if (!AreSameSize(*this, other)) throw std::invalid_argument("The vectors don't have the same size!");
		if (!(length == 3 || length == 2))
			throw std::invalid_argument(
				"The cross product function is defined only for 3D and 2D.");

		// 3D -> 3D vector perpendicular to both | 2D -> 3D vector whose Z coordinate is the area of signed parallelogram
		return TVector(
			this->Y() * other.Z() - this->Z() * other.Y(),
			this->Z() * other.X() - this->X() * other.Z(),
			this->X() * other.Y() - this->Y() * other.X());
	}

	float TVector::Angle(const TVector& other) const
	{
		const float dot = this->Dot(other);
		const float rad2deg = 180 / 3.1415926535f;
		return acos(dot / (this->Magnitude() * other.Magnitude())) * rad2deg;
	}

	float TVector::Magnitude() const
	{
		float squaredMagnitude = 0.0f;
		for (float c : Data) squaredMagnitude += pow(c, 2);
		return sqrtf(squaredMagnitude);
	}

	TVector TVector::Normalized() const
	{
		const float magnitude = Magnitude();

		std::vector<float> vars;
		for (float c : Data) vars.push_back(c / magnitude);
		return TVector(vars);
	}

	TVector TVector::Inverted() const
	{
		return *this * -1.0f;
	}

	TVector TVector::Clamp(const float maxLength) const
	{
		if (Magnitude() <= maxLength) return *this;
		else return Normalized() * maxLength;
	}

	TVector TVector::Scale(const TVector& other) const
	{
		if (!AreSameSize(*this, other)) throw std::invalid_argument("The vectors don't have the same size!");

		std::vector<float> vars;
		for (int i = 0; i < GetLength(); ++i)
		{
			float c = (*this)[i] * other[i];
			vars.push_back(c);
		}
		return TVector(vars);
	}

	TVector TVector::Towards(const TVector& point) const
	{
		// Assuming 'this' is also a point
		return (point - *this);
	}

	float TVector::Distance(const TVector& point) const
	{
		// Assuming 'this' is also a point
		return Towards(point).Magnitude();
	}

	float TVector::Sum() const
	{
		float sum = 0;
		for (int i = 0; i < GetLength(); ++i)
		{
			sum += (*this)[i];
		}
		return sum;
	}

	TVector TVector::GetTexCoords(const float radius) const
	{
		if (Data.size() < 3) throw std::invalid_argument("The vector is expected to have XYZ coordinates.");

		const auto s = (Data[0] / radius + 1) / 2.0f;
		const auto r = (Data[2] / radius + 1) / 2.0f;
		return TVector(s, r, 0);
	}

	TVector TVector::Reflect(const TVector& normal) const
	{
		if (Mathf::Approximately(normal.Magnitude(), 0)) return this->Inverted(); // zero normal vector
		
		const auto n = normal.Normalized();		
		return *this - 2.0f * this->Dot(n) * n;
	}

	bool TVector::IsBetween(const TVector& a, const TVector& c) const
	{
		// https://stackoverflow.com/questions/13640931/how-to-determine-if-a-vector-is-between-two-other-vectors
		return a.Cross(*this).Dot(a.Cross(c)) >= 0 && c.Cross(*this).Dot(c.Cross(a)) >= 0;
	}

	TVector TVector::Flatten() const
	{
		vector<float> newData(Data);
		newData[1] = 0.0f; // Fixed 'Y' direction
		return TVector(newData);
	}

	TVector TVector::Interpolate(const TVector& other, float amount) const
	{
		amount = std::clamp(amount, 0.0f, 1.0f);
		return *this * amount + (1 - amount) * other;
	}

#pragma endregion Math
}