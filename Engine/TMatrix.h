#pragma once

#include <vector>
#include "TVector.h"

namespace Game::Engine
{
	/**
	 * Representation of sqaure matrices consisting of vectors of equal length.
	 * Only 3x3 matrices are currently supported.
	 */
	class TMatrix
	{
	private:
		std::vector<TVector> Data{};
	public:
		const size_t Size = 3; //! Expected size
#pragma region Constructors

		const std::string NotSquareError = "The matrix you're attempting to create isn't 3x3 square!";

		/* Constructor of an empty 3�3 matrix. */
		TMatrix();

		explicit TMatrix(const std::initializer_list<TVector> vectors) : Data(vectors)
		{
			if (!IsSquare()) throw std::invalid_argument(NotSquareError);
		}

		/* Constructs a matrix from a 3x3 array. */
		explicit TMatrix(const float m[3][3]) : TMatrix({
			{m[0][0], m[0][1], m[0][2]},
			{m[1][0], m[1][1], m[1][2]},
			{m[2][0], m[2][1], m[2][2]} }) { }

		explicit TMatrix(const std::vector<TVector>& vectors)
		{
			Data.clear();
			for (const auto& v : vectors)
			{
				Data.push_back(v);
			}
			if (!IsSquare()) throw std::invalid_argument(NotSquareError);
		}

		explicit TMatrix(const std::initializer_list<std::initializer_list<float>> floatVectors)
		{
			Data.clear();
			for (const auto floats : floatVectors)
			{
				auto v = TVector(floats);
				Data.push_back(v);
			}
			if (!IsSquare()) throw std::invalid_argument(NotSquareError);
		}

#pragma endregion Constructors

#pragma region Common matrices

		/* Constructs an empty matrix filled with all ones. */
		static TMatrix Ones(int dimension = 3);

		/* Constructs an empty matrix filled with all zeros. */
		static TMatrix Zeros(const int dimensions = 3)
		{
			return Ones(dimensions) * 0;
		}
		/** Constructs a rotational matrix from the rotational axis and given amount of degrees. */
		static TMatrix Rotation(const TVector& axis, float degrees);

#pragma endregion Common matrices

#pragma region Iterator
		using iterator = std::vector<TVector>::iterator;
		using const_iterator = std::vector<TVector>::const_iterator;

		iterator begin() { return Data.begin(); }
		iterator end() { return Data.end(); }
		[[nodiscard]] const_iterator cbegin() const { return Data.cbegin(); }
		[[nodiscard]] const_iterator cend() const { return Data.cend(); }
#pragma endregion Iterator

#pragma region Getters and Setters
		/** Returns the width of the matrix (length of rows). */
		[[nodiscard]] size_t GetWidth() const { return Data[0].GetLength(); }
		/** Returns the height of the matrix (length of columns). */
		[[nodiscard]] size_t GetHeight() const { return Data.size(); }
		/** Returns an i-th row of the matrix. */
		[[nodiscard]] TVector GetRow(const size_t i) const { return Data[i]; }
		/** Returns an i-th column of the matrix. */
		[[nodiscard]] TVector GetColumn(const size_t i) const { return Transposed().Data[i]; }

		/** Checks if the width of the matrix matches its height. */
		[[nodiscard]] bool IsSquare() const
		{
			const size_t height = Data.size();
			if (height != 3) return false;
			for (const auto& vector : Data) if (vector.GetLength() != 3) return false;
			return true;
		}

#pragma endregion Getters and Setters

#pragma region Operators
		/** Returns an i-th row of the matrix. */
		TVector operator [](const std::size_t i) const { return GetRow(i); }

		/** String representation of a matrix. */
		[[nodiscard]] std::string ToString() const;

		TMatrix& operator=(const TMatrix other)
		{
			this->Data = other.Data;
			return *this;
		}

		/** Matrix addition. */
		TMatrix& operator +=(const TMatrix& other);

		/** Matrix addition. */
		friend TMatrix operator +(TMatrix a, const TMatrix& b)
		{
			// friends defined inside class body are inline and are hidden from non-ADL lookup
			a += b; // reuse compound assignment
			return a; // return the result by value (uses move constructor)
		}

		/** Matrix substraction. */
		TMatrix& operator -=(const TMatrix& other)
		{
			const TMatrix opposite = (-1.0f * other);
			*this += opposite;
			return *this;
		}

		/** Matrix substraction. */
		friend TMatrix operator -(TMatrix a, const TMatrix& b)
		{
			a -= b;
			return a;
		}

		/** Matrix multiplication by a scalar value. */
		TMatrix& operator *=(const float& scalar);

		/** Matrix multiplication by a scalar value. */
		friend TMatrix operator *(TMatrix a, const float& b)
		{
			a *= b;
			return a;
		}
		
		/** Matrix multiplication by a scalar value. */
		friend TMatrix operator *(const float& b, TMatrix a)
		{
			a *= b;
			return a;
		}

		/** Matrix multiplication by a vector. */
		TVector operator *(const TVector& vector) const;

		/** Matrix multiplication by another matrix. */
		TMatrix& operator *=(const TMatrix& other);

		/** Matrix multiplication by another matrix. */
		friend TMatrix operator *(TMatrix a, const TMatrix& b)
		{
			a *= b;
			return a;
		}

		/** The vectors are practically equal. */
		bool operator ==(const TMatrix& other) const;
		/** The vectors are not equal. */
		bool operator!=(const TMatrix& other) const
		{
			return !(*this == other);
		}
#pragma region Operators

#pragma region Math
		/** Swaps columns and rows in the matrix. */
		[[nodiscard]] TMatrix Transposed() const;

		/** Constructs an inverse matrix. */
		[[nodiscard]] TMatrix Inverted() const;

		/** Calculates the determinant of the matrix. */
		[[nodiscard]] float Determinant() const;

#pragma endregion Math
	};

	/** String representation of a vector. */
	inline std::ostream& operator<<(std::ostream& str, TMatrix const& m)
	{
		return str << m.ToString();
	}
}
