﻿#pragma once

#include "TVector.h"

namespace Game::Engine
{
	class TRay
	{
	public:
		/* The origin point of the ray. */
		TVector Origin;
		/* The direction of the ray. */
		TVector Direction;

#pragma region Constructors
		/* Creates a ray starting at origin along a direction. */
		explicit TRay(const TVector& origin, const TVector& direction)
		{
			Origin = origin;
			Direction = direction.Normalized();
		}

		/* Creates a ray starting at the world origin along a direction. */
		explicit TRay(const TVector& direction) : TRay(TVector({0,0,0}), direction) {}

		/* Constructor of an empty ray leading nowhere. */
		TRay()
		{
			Origin = TVector();
			Direction = TVector();
		}

		/* Constructor of a ray of arbitrary length. */
		explicit TRay(const std::initializer_list<float> origin, const std::initializer_list<float> direction)
		{
			if (origin.size() != direction.size()) 
				throw std::invalid_argument("Your ray must be made of vectors of the same size.");
			Origin = TVector(origin);
			Direction = TVector(direction).Normalized();
		}
#pragma endregion Constructors

#pragma region Math
		/* Calculates the distance between two rays. */
		[[nodiscard]] float Distance(const TRay& ray) const;

		/* Calculates the distance between a ray and a point. */
		[[nodiscard]] float Distance(const TVector& point) const;

		/* Returns a point at 'distance' units along the ray. */
		[[nodiscard]] TVector GetPoint(float distance) const;

		[[nodiscard]] std::string ToString() const { return Direction.ToString(); }
#pragma endregion Math


	};
}
