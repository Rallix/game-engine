#pragma once

#include <vector>
#include <random>
#include <chrono>
#include "TVector.h"

namespace Game::Engine
{
	using std::vector;

	class Mathf
	{
	public:
		static constexpr float PI = 3.14159265358979f;
		static constexpr float DEG_2_RAD = (PI * 2) / 360;
		static constexpr float RAD_2_DEG = 360 / (PI * 2);
		/* The maximal difference between floating point values for the vectors to be considered equal.	*/
		static constexpr float EPSILON = 0.000'001f;

		/* Checks if two floating point numbers are practically identical. */
		static bool Approximately(const float a, const float b)
		{
			return a >= b - EPSILON && a <= b + EPSILON;
		}

		/* Calculates the determinant of 2x2  */
		static float Det2X2(const float topleft, const float topright, const float bottomleft, const float bottomright)
		{
			return bottomright * topleft - bottomleft * topright;
		}

		/** Returns the coordinates of a circle on the XZ plane with a given radius. */
		static vector<TVector> GetCircleCoords(const float radius, const size_t points = 32, const TVector& center = TVector(0, 0, 0))
		{
			vector<TVector> coordinates;

			float theta = 0.0f;
			const float step = PI * 2 / static_cast<float>(points);
			for (size_t i = 0; i < points; i++) {
				const float x = radius * cos(theta);
				const float y = radius * sin(theta);
				TVector position = center + TVector(x, 0, y);
				theta += step;

				coordinates.push_back(position);
			}
			return coordinates;
		}

		/** Generates a nice random number in given range. */
		static float Random(float from, float to)
		{
			std::mt19937_64 random;
			
			// Initialize the random number generator with time-dependent seed
			const uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
			std::seed_seq ss{ uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32) };
			random.seed(ss);
			
			// Initialize a uniform distribution between the given numbers
			const std::uniform_real_distribution<double> unif(from, to);
			
			return static_cast<float>(unif(random));
		}

		/** Generates a nice random numbers in given range, and uses them to construct a direction. */
		static TVector RandomDirection(const float from, const float to)
		{
			return TVector(Random(from, to), Random(from, to), Random(from, to)).Flatten().Normalized();
		}
	};
}