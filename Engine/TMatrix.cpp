#pragma once

#include "TMatrix.h"
#include "../Engine/Mathf.cpp"

// (?) Rework from vectors to 3x3 multidimensional array if it's slow
// Quaternions for rotations (...or rather not)

namespace Game::Engine
{
#pragma region Constructors

	TMatrix::TMatrix()
	{
		Data = Zeros(3).Data; //! 3�3 matrix only
	}

#pragma endregion Constructors
#pragma region Common matrices

	TMatrix TMatrix::Ones(const int dimension)
	{
		if (dimension < 2) throw std::invalid_argument("The matrix must have at least dimension 2x2.");
		std::vector<TVector> vectors;
		vectors.reserve(dimension);
		for (int d = 0; d < dimension; ++d)
		{
			vectors.push_back(TVector::Ones(dimension));
		}
		return TMatrix(vectors);
	}

	TMatrix TMatrix::Rotation(const TVector& axis, const float degrees)
	{
		//! See here: https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
		
		const TVector u = axis.Normalized(); // normalized rotational axis
		const float th = Mathf::DEG_2_RAD * degrees; // theta angle in radians

		const float sinTh = sin(th);
		const float cosTh = cos(th);
		const float cos1_Th = 1 - cosTh;

		auto a = cosTh + u.X()*u.X()*cos1_Th;
		auto b = u.X()*u.Y()*cos1_Th - u.Z()*sinTh;
		auto c = u.X()*u.Z()*cos1_Th + u.Y()*sinTh;

		auto d = u.Y()*u.X()*cos1_Th + u.Z()*sinTh;
		auto e = cosTh + u.Y()*u.Y()*cos1_Th;
		auto f = u.Y()*u.Z()*cos1_Th - u.X()*sinTh;

		auto g = u.Z()*u.X()*cos1_Th - u.Y()*sinTh;
		auto h = u.Z()*u.Y()*cos1_Th + u.X()*sinTh;
		auto i = cosTh + u.Z()*u.Z()*cos1_Th;
		
		const auto row0 = TVector(a, b, c);
		const auto row1 = TVector(d, e, f);
		const auto row2 = TVector(g, h, i);
		return TMatrix({row0, row1, row2});
	}
#pragma endregion Common matrices
#pragma region Operators

	std::string TMatrix::ToString() const
	{
		std::string s;
		s.append("{");
		for (size_t i = 0; i < this->GetHeight() - 1; i++)
		{
			s.append(this->Data[i].ToString());
			s.append(", ");
		}
		s.append(this->Data[this->GetHeight() - 1].ToString());
		s.append("}");
		return s;
	}

	TMatrix& TMatrix::operator+=(const TMatrix& other)
	{
		for (size_t i = 0; i < this->GetHeight(); i++)
		{
			this->Data[i] += other.Data[i];
		}
		return *this;
	}

	TMatrix& TMatrix::operator*=(const float& scalar)
	{
		for (size_t i = 0; i < this->GetHeight(); i++)
		{
			this->Data[i] *= scalar;
		}
		return *this;
	}
	
	TVector TMatrix::operator*(const TVector& vector) const
	{		
		const auto result = TVector(Data[0].Dot(vector), Data[1].Dot(vector), Data[2].Dot(vector));
		return result;
	}

	TMatrix& TMatrix::operator*=(const TMatrix& other)
	{
		if (this->GetHeight() != 3 || other.GetHeight() != 3) throw std::invalid_argument("Multiplication is only supported for 3x3 matrices..");
		/*
		auto result = TMatrix(this->GetHeight(), other.GetWidth());
		if (this->GetWidth() != other.GetHeight()) throw std::invalid_argument("Multiplication of these two matrices isn't possible.");
		for (size_t i = 0; i < this->GetHeight(); i++) // Rows of A
		{
			for (size_t j = 0; j < this->GetWidth(); j++) // Columns of B
			{
				for (size_t k = 0; k < this->GetWidth(); k++) // Rows of B
				{
					result[i][j] = (this[i][k] * other[k][j]).Sum();
				}
			}
		}*/

		//! Fast multiplication (fewer multiplications and additions are performed)
		const float c11 = this->Data[0][0] * other[0][0] + this->Data[0][1] * other[1][0] + this->Data[0][2] * other[2][0];
		const float c12 = this->Data[0][0] * other[0][1] + this->Data[0][1] * other[1][1] + this->Data[0][2] * other[2][1];
		const float c13 = this->Data[0][0] * other[0][2] + this->Data[0][1] * other[1][2] + this->Data[0][2] * other[2][2];
		const float c21 = this->Data[1][0] * other[0][0] + this->Data[1][1] * other[1][0] + this->Data[1][2] * other[2][0];
		const float c22 = this->Data[1][0] * other[0][1] + this->Data[1][1] * other[1][1] + this->Data[1][2] * other[2][1];
		const float c23 = this->Data[1][0] * other[0][2] + this->Data[1][1] * other[1][2] + this->Data[1][2] * other[2][2];
		const float c31 = this->Data[2][0] * other[0][0] + this->Data[2][1] * other[1][0] + this->Data[2][2] * other[2][0];
		const float c32 = this->Data[2][0] * other[0][1] + this->Data[2][1] * other[1][1] + this->Data[2][2] * other[2][1];
		const float c33 = this->Data[2][0] * other[0][2] + this->Data[2][1] * other[1][2] + this->Data[2][2] * other[2][2];
		auto result = TMatrix({ {c11, c12, c13}, {c21, c22, c23}, {c31, c32, c33} });

		Data.clear();
		Data.emplace_back(TVector(c11, c12, c13));
		Data.emplace_back(TVector(c21, c22, c23));
		Data.emplace_back(TVector(c31, c32, c33));
		return *this;
	}

	bool TMatrix::operator==(const TMatrix& other) const
	{
		for (size_t i = 0; i < this->GetHeight(); i++)
		{
			if (this->Data[i] != other.Data[i]) return false;
		}
		return true;
	}



#pragma endregion Operators
#pragma region Math

	TMatrix TMatrix::Transposed() const
	{
		const auto row0 = TVector(Data[0][0], Data[1][0], Data[2][0]);
		const auto row1 = TVector(Data[0][1], Data[1][1], Data[2][1]);
		const auto row2 = TVector(Data[0][2], Data[1][2], Data[2][2]);
		return TMatrix({row0, row1, row2});
	}

	TMatrix TMatrix::Inverted() const
	{
		//? See here: https://www.mathsisfun.com/algebra/matrix-inverse-minors-cofactors-adjugate.html
		
		//! Matrix of minors
		float minors[3][3];
		for (size_t row = 0; row < 3; row++)
		{
			for (size_t col = 0; col < 3; col++)
			{
				// All *except* the current row & column
				const int r1 = (row == 0) ? 1 : 0;
				const int c1 = (col == 0) ? 1 : 0;
				const int r2 = (row == 2) ? 1 : 2;
				const int c2 = (col == 2) ? 1 : 2;
				minors[row][col] = Mathf::Det2X2(Data[r1][c1], Data[r1][c2], 
												Data[r2][c1], Data[r2][c2]);
			}
		}
		//! Matrix of cofactors
		const float negate = -1;
		minors[0][1] *= negate;
		minors[1][0] *= negate;
		minors[1][2] *= negate;
		minors[2][1] *= negate;
		//! Determinant of the matrix
		const float det = Data[0][0] * minors[0][0] + Data[0][1] * minors[0][1] + Data[0][2] * minors[0][2];

		const TMatrix adjugate = TMatrix(minors).Transposed();
		return (1/det) * adjugate;
	}

	float TMatrix::Determinant() const
	{
		auto det2 = [](const float tl, const float tr, const float bl, const float br)
		{
			return br * tl - bl * tr;
		};
		const float a = Data[0][0]; const float b = Data[1][0]; const float c = Data[2][0];
		const float d = Data[0][1]; const float e = Data[1][1]; const float f = Data[2][1];
		const float g = Data[0][2]; const float h = Data[1][2]; const float i = Data[2][2];
		return a * det2(e, f, h, i)
			- b * det2(d, f, g, i)
			+ c * det2(d, e, g, h);
	}


#pragma endregion Math
}