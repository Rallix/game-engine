#pragma once

#include <vector>
#include <ostream>

namespace Game::Engine
{
	using std::vector;

	/**
	 * Representation of vectors and points of any dimension, defaulting to 3D.
	 */
	class TVector
	{		
	private:
		/* The component data container. */
		vector<float> Data;		
	public:

#pragma region Constructors
		/* Constructor of a two-component vector. */
		TVector(float x, float y);

		/* Constructor of a three-component zero vector. */
		TVector();

		/* Constructor of a three-component vector. */
		TVector(float x, float y, float z);

		/* Constructor of a four-component vector. */
		TVector(float x, float y, float z, float w);

		/* Constructor of a vector of arbitrary length. */
		explicit TVector(const std::initializer_list<float> vector) : Data(vector)
		{
			if (Data.size() < 2) throw std::invalid_argument("The vector must have at least two components.");		
		}

		explicit TVector(const std::vector<float>& components) : Data(components)
		{
			if (Data.size() < 2) throw std::invalid_argument("The vector must have at least two components.");
			Data.clear();
			for (float c : components)
			{
				Data.push_back(c);
			}
		}

#pragma endregion Constructors

#pragma region Common vectors

		/** Three-dimensional vector with all ones. */
		static TVector Ones(const int dimension = 3)
		{
			if (dimension < 2) throw std::invalid_argument("The vector must have at least two components.");
			TVector v = TVector({1, 1});
			for (int d = 3; d <= dimension; ++d)
			{
				v.Data.push_back(1);
			}
			return v;
		}
		
		/** Three-dimensional vector with all zeros. */
		static TVector Zeros(const int dimension = 3)
		{
			if (dimension < 2) throw std::invalid_argument("The vector must have at least two components.");
			TVector v = TVector({0, 0});
			for (int d = 3; d <= dimension; ++d)
			{
				v.Data.push_back(0);
			}
			return v;
		}
		
		/** Upward three-dimensional unit vector. */
		static TVector Up() { return TVector(0, 1, 0);}
		/** Downward three-dimensional unit vector. */
		static TVector Down() { return TVector(0, -1, 0);}
		/** Leftward three-dimensional unit vector. */
		static TVector Left() { return TVector(-1, 0, 0);}
		/** Rightward three-dimensional unit vector. */
		static TVector Right() { return TVector(1, 0, 0);}
		/** Forward three-dimensional unit vector. */
		static TVector Forward() { return TVector(0, 0, 1);}
		/** Backward three-dimensional unit vector. */
		static TVector Backward() { return TVector(0, 0, -1);}
#pragma endregion 

#pragma region Iterator
		// Overloading iterators | https://stackoverflow.com/a/36405824/10806555
		using iterator = std::vector<float>::iterator;
		using const_iterator = std::vector<float>::const_iterator;

		iterator begin() { return Data.begin(); }
		iterator end() { return Data.end();	}
		[[nodiscard]] const_iterator cbegin() const {	return Data.cbegin(); }
		[[nodiscard]] const_iterator cend() const	{ return Data.cend(); }
#pragma endregion Iterator

#pragma region Getters and Setters
		/** Returns the length of the vector. */
		[[nodiscard]] int GetLength() const { return static_cast<int>(Data.size()); }
		/** Returns the first component of the vector. */
		[[nodiscard]] float X() const { return GetLength() >= 1 ? Data[0] : 0; }
		/** Returns the second component of the vector. */
		[[nodiscard]] float Y() const { return GetLength() >= 2 ? Data[1] : 0; }
		/** Returns the third component of the vector. */
		[[nodiscard]] float Z() const { return GetLength() >= 3 ? Data[2] : 0; }
		/** Returns the fourth component of the vector. */
		[[nodiscard]] float W() const { return GetLength() >= 4 ? Data[3] : 0; }
		/** Returns the first three components of the vector. */
		[[nodiscard]] TVector XYZ() const { return TVector(X(), Y(), Z()); }
		/** Returns an arbitrary component of the vector. */
		[[nodiscard]] float Get(const int i) const { return GetLength() > i ? Data[i] : 0; }
		/** Returns the first three components of the vector. */
		[[nodiscard]] TVector STR(const float radius) const { return GetTexCoords(radius); }
#pragma endregion Getters and Setters

#pragma region Operators

		/** Returns a vector component on given index. */
		float operator [](const int i) const { return Get(i); }
		
		/** Vector addition which returns the diagonal of the parallelogram formed between them. */
		TVector& operator +=(const TVector& other);

		/** Vector addition which returns the diagonal of the parallelogram formed between them. */
		friend TVector operator +(TVector a, const TVector& b)
		{
			// friends defined inside class body are inline and are hidden from non-ADL lookup
			a += b; // reuse compound assignment
			return a; // return the result by value (uses move constructor)
		}

		/** Vector substraction which returns the distance from 'other' to the source vector. */
		TVector& operator -=(const TVector& other)
		{
			const TVector opposite = (-1.0f * other);
			*this += opposite;
			return *this;
		}

		/** Vector substraction which returns the distance from 'other' to the source vector. */
		friend TVector operator -(TVector a, const TVector& b)
		{
			a -= b;
			return a;
		}

		/** Vector multiplication by a scalar value which makes it shorter or longer. */
		TVector& operator *=(const float& scalar);

		/** Vector multiplication by a scalar value which makes it shorter or longer. */
		friend TVector operator *(TVector a, const float& b)
		{
			a *= b;
			return a;
		}

		/** Vector multiplication by a scalar value which makes it shorter or longer. */
		friend TVector operator *(const float& b, TVector a)
		{
			a *= b;
			return a;
		}

		/** Vector multiplication by another vector results in element-wise multiplication. */
		TVector& operator *=(const TVector& other);

		/** Vector multiplication by another vector results in element-wise multiplication. */
		friend TVector operator *(TVector a, const TVector& b)
		{
			a *= b;
			return a;
		}

		/** The vectors are practically equal. */
		bool operator ==(const TVector& other) const;
		/** The vectors are not equal. */
		bool operator!=(const TVector& other) const
		{
			return !(*this == other);
		}

#pragma endregion Operators

		/** Checks if the dimensions of two vectors match. */
		static bool AreSameSize(const TVector& a, const TVector& b);

		/** String representation of a vector. */
		[[nodiscard]] std::string ToString() const;

#pragma region Math

		/** Calculates the dot-product of the two vectors (relationship between their directions). */
		[[nodiscard]] float Dot(const TVector& other) const;

		/** Calculates the cross-product of the two vectors (a vector perpendicular to both).
		 * In 2D, the length of the vector is equal to the area of a signed parallelogram. */
		[[nodiscard]] TVector Cross(const TVector& other) const;

		/** Calculates the angle between two vectors in degrees. */
		[[nodiscard]] float Angle(const TVector& other) const;

		/** Calculates the length of the vector. */
		[[nodiscard]] float Magnitude() const;

		/** Returns a normalized, unit-sized vector. */
		[[nodiscard]] TVector Normalized() const;

		/** Returns an inverted vector to point in the opposite direction. */
		[[nodiscard]] TVector Inverted() const;

		/** Returns a vector clamped to a specific magnitude. */
		[[nodiscard]] TVector Clamp(float maxLength) const;

		/** Multiplies two vectors component-wise. */
		[[nodiscard]] TVector Scale(const TVector& other) const;

		/** Returns the distance between two points. */
		[[nodiscard]] float Distance(const TVector& point) const;

		/** Returns a vector pointing towards a point. */
		[[nodiscard]] TVector Towards(const TVector& point) const;

		/** Returns a sum of the vector elements. */
		[[nodiscard]] float Sum() const;

		/** Converts regular position coordinates into texture coordinates. */
		[[nodiscard]] TVector GetTexCoords(float radius) const;

		/** Reflects a vector alongside a normal. */
		[[nodiscard]] TVector Reflect(const TVector& normal) const;

		/** Checks if a vector is located between two other vectors (same plane). They ought to be coming from the same point. */
		[[nodiscard]] bool IsBetween(const TVector& a, const TVector& c) const;

		/** Zeroes-out the Y coordinate of the vector. */
		[[nodiscard]] TVector Flatten() const;

		/** Interpolates from one vector to the other. */
		[[nodiscard]] TVector Interpolate(const TVector& other, float amount) const;

#pragma endregion Math
	}; //! TVector class

	/** String representation of a vector. */
	inline std::ostream& operator<<(std::ostream& str, TVector const& v)
	{
		return str << v.ToString();
	}
} //! Game namespace
