#include <string>

#include "Game.h"
#include "../Game/Properties.h"
#include "../Game/Input.h"
#include "../Game/Menu.h"
#include "../Game/TGround.h"
#include "../Game/TGround.cpp" // include cpp to avoid LNK2019 errors
#include "../Game/GameInfo.h"
#include "../Game/TPaddle.h"
#include "../Game/TPaddle.cpp"
#include "../Game/TBall.h"
#include "../Game/TBall.cpp"
#include "../Game/TBrick.h"
#include "../Game/TBrick.cpp"
#include "../Game/TWall.h"
#include "../Game/TWall.cpp"

#define IL_USE_PRAGMA_LIBS
#include "IL/devil_cpp_wrapper.hpp"
// Avoid false positives errors
#ifndef IL_ORIGIN_SET
#define IL_ORIGIN_SET        0x0600
#endif
#ifndef IL_ORIGIN_LOWER_LEFT
#define IL_ORIGIN_LOWER_LEFT 0x0601
#endif

namespace Game::Breakout
{
	using namespace Game::Breakout;

	// Camera rotation
	float rotation = 0;
	int collisionCheckPeriod = 0;

	//! Game components
	const int Radius = 64;
	TGround Ground(Radius, Radius / 2);
	vector<TPaddle> Paddles;
	const auto PaddleRadii = std::make_pair(54.0f, 60.0f); 
	TBall Ball(2.5f);

	float randomness = 0.0f;
	const float MaxRandomness = 0.1f;

	const int bricks = 12;
	//TWall Castle({bricks, bricks * 1.5f}, bricks / 2.0f, bricks, 4);
	int specialBrick = static_cast<int>(std::round(Mathf::Random(0, 12)));
	TWall Castle({ 16.0f, 21.0f }, 6, 12, 4, specialBrick);

	//! Textures	
	GLuint stone;
	GLuint grass;

	// x: 1.	Paddles
	// x: 1.1	Keyboard input
	// x: 2.	Ball
	// x: 2.1	Collision detection
	// x: 2.1.1		Outer walls
	// x: 2.1.2		Paddles
	// x: 2.2	Bounce
	// x: 2.3	Fine-tune
	// x: 3.	Bricks	
	// x: 3.1	Single brick
	// x: 3.2	Brick castle (ramparts)
	// x: 4. Score and victory messages
	// x: 5.	Cameras & lights
	// x: 5.1	Perspective, Top, Ball
	// x: 5.2	Ambient, Specular, Diffuse

#undef main
	int Main(int argc, char** argv)
	{
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

		glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH) - Width) / 2, (glutGet(GLUT_SCREEN_HEIGHT) - Height) / 2);
		glutInitWindowSize(Width, Height);
		glutCreateWindow("Castle Siege | 3D Breakout");

		//! Input
		glutKeyboardFunc(GetKeyDown);
		glutKeyboardUpFunc(GetKeyUp);
		glutWarpPointer(Mouse[0], Mouse[1]);

		//! Right click menu
		glutCreateMenu(RightClickMenu);
		glutAddMenuEntry("New Game", 1);
		glutAddMenuEntry("Exit", 2);
		glutAttachMenu(GLUT_RIGHT_BUTTON);

		//! Render
		glutDisplayFunc(Render);
		glutReshapeFunc(Reshape);

		Start();
		glutTimerFunc(deltaTime, Loop, 0);
		glutMainLoop();
		return 0;
	}

	void Render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glEnable(GL_LIGHT2);

		//! Camera views
		// gluLookAt( { camera position }, { target position }, { roll });
		switch (Game.Camera) {
		default:
		case CameraMode::Perspective:
			gluLookAt(0, 100, 100, 0, 0, 18, 0, 1, 0);
			break;
		case CameraMode::Top:
			gluLookAt(0, 160, 1, 0, 0, 0, 0, 1, 0);
			break;
		case CameraMode::Ball:
			const float offset = 30;
			auto behindBall = (Ball.Velocity * -1.0f).Normalized() * offset;
			if (behindBall == TVector()) behindBall = TVector().Towards(Ball.Position).Normalized() * offset;
			behindBall = Ball.Position + behindBall; // <-- this is confusing without smoothing (because velocity changes instantly)
			gluLookAt(Ball.Position[0], 75, Ball.Position[1], Ball.Position[0], Ball.Position[1], Ball.Position[2], 0, 1, 0);
			break;
		}

		glRotatef(rotation, 0, 1, 0);

		// const float light0[] = { 0, 60, 0, 1 };
		// glLightfv(GL_LIGHT0, GL_POSITION, light0);

		const float light1[] = { 0, 60, 7.5f, 1 };
		const float light1Direction[] = { 0, -1, -0.2f };
		glLightfv(GL_LIGHT1, GL_POSITION, light1);
		glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, light1Direction);

		// towards center
		float light2[] = { 0, 45, 75, 1 };
		float light2Direction[] = { -light2[0], -light2[1], -light2[2] };
		glLightfv(GL_LIGHT2, GL_POSITION, light2);
		glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light2Direction);

		RenderComponents();

		//! UI
		glDisable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
		glDisable(GL_LIGHTING);

		// UI --> 2D orthogonal camera
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		gluOrtho2D(0.0, Width, Height, 0.0);

		// Text
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glColor3d(White[0], White[1], White[2]);
		const auto leftAlign = [](const int width, const int offset = 0) { return double(width) * 0.025 + offset; };
		const auto topAlign = [](const int height, const int offset = 0) { return double(height) * 0.05 + offset; };

		const int lineHeight = 28;
		if (Game.State == GameState::Ready) {
			IDrawable::ShowText(leftAlign(Width), topAlign(Height), "Right click:", Green);
			IDrawable::ShowText(leftAlign(Width, 100), topAlign(Height), "Menu (Start a new game)");

			IDrawable::ShowText(leftAlign(Width), topAlign(Height, lineHeight), "A & D:", Red);
			IDrawable::ShowText(leftAlign(Width, 60), topAlign(Height, lineHeight), "Move the paddles");

			IDrawable::ShowText(leftAlign(Width), topAlign(Height, lineHeight * 2), "Space:", Red);
			IDrawable::ShowText(leftAlign(Width, 63), topAlign(Height, lineHeight * 2), "Release the ball | Confirm");

			IDrawable::ShowText(leftAlign(Width), topAlign(Height, lineHeight * 3), "1, 2, 3:", Red);
			IDrawable::ShowText(leftAlign(Width, 63), topAlign(Height, lineHeight * 3), "Cameras");
		}
		else
		{
			if (Game.State == GameState::Won)
			{
				IDrawable::ShowText(leftAlign(Width), topAlign(Height), "You win!", Green);
			}
			else if (Game.State == GameState::Lost)
			{
				IDrawable::ShowText(leftAlign(Width), topAlign(Height), "You lose!", Red);
			}
			else {
				IDrawable::ShowText(leftAlign(Width), topAlign(Height), "Lives:", Green);
				IDrawable::ShowText(leftAlign(Width, 63), topAlign(Height), std::to_string(Game.Lives));
			}

			IDrawable::ShowText(leftAlign(Width), topAlign(Height, lineHeight), "Time:", Cyan);
			IDrawable::ShowText(leftAlign(Width, 63), topAlign(Height, lineHeight), std::to_string(Game.GetElapsedTime()));

			IDrawable::ShowText(leftAlign(Width), topAlign(Height, lineHeight*2), "Speed:", Cyan);
			IDrawable::ShowText(leftAlign(Width, 63), topAlign(Height, lineHeight*2), std::to_string(Ball.Speed));

			IDrawable::ShowText(leftAlign(Width), topAlign(Height, lineHeight*3), "Randomness:", Cyan);
			IDrawable::ShowText(leftAlign(Width, 112), topAlign(Height, lineHeight*3), std::to_string(randomness));
		}

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();

		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		glEnable(GL_LIGHTING);

		glutSwapBuffers();
	}

	static const std::wstring TexturePath = L"./tex/";
#define TEXTURE(file) (TexturePath + (file)).c_str() // macro, because wchar_t* can't be returned by a function
	static void LoadTexture(GLuint* texture, const std::wstring& file)
	{
		ilImage image;
		image.Load(TEXTURE(file));
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.Width(), image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, image.GetData());
	}

	void Reshape(const int newWidth, const int newHeight)
	{
		Width = newWidth;
		Height = newHeight;
		glViewport(0, 0, Width, Height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		
		const float fieldOfView = 45; //! 60?
		const float aspectRatio = float(Width) / float(Height);
		gluPerspective(fieldOfView, aspectRatio, 1.0f, 1000.0);
		glMatrixMode(GL_MODELVIEW);
	}

	void ResetBall() // Shifts the ball in front of the paddle and resets the velocity.
	{
		const auto paddleCenter = Paddles[2].Points[0] + Paddles[2].Points[0].Towards(Paddles[2].Points[3]) * 0.5f;
		const auto paddleDirection = TVector().Towards(paddleCenter).Normalized();

		Ball.IsSpecial = false;				
		Ball.Speed = TBall::MinSpeed;
		Ball.Position = paddleDirection.Normalized() * 40;
		randomness = 0;
		TVector randomDirection;
		do
		{
			randomDirection = Mathf::RandomDirection(-1, 1).Normalized(); // Don't start by going sideways
		} while (abs(randomDirection.Dot(TVector().Towards(Ball.Position))) < 0.25f);
		randomDirection = randomDirection.Interpolate(TVector().Towards(Ball.Position), 0.75f);
		Ball.AddForce(randomDirection, TBall::MinSpeed); //! won't move until un'Paused'	
	}

	void Start()
	{
		//! Initialize interactive components
		// Paddle
		Paddles.clear();
		const int paddlesCount = 3;
		for (int p = 0; p < paddlesCount; p++)
		{			
			TPaddle paddle = TPaddle(Brown, PaddleRadii, 9, 16);
			paddle.Turn((360.0f / float(paddlesCount)) * float(p));
			Paddles.push_back(paddle);
		}
		// Ball		
		ResetBall();

		glLineWidth(2.5f);
		glClearColor(BackgroundColor[0], BackgroundColor[1], BackgroundColor[2], BackgroundColor[3]);

		glShadeModel(GL_SMOOTH);

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		glEnable(GL_COLOR_MATERIAL);

		// glLightfv(GL_LIGHT0, GL_AMBIENT, White);
		// glEnable(GL_LIGHT0);

		glLightfv(GL_LIGHT1, GL_DIFFUSE, Grey);
		glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 175); // 175
		glLightf(GL_LIGHT1, GL_SPOT_EXPONENT, 16);
		glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0);
		glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.0003f);
		glEnable(GL_LIGHT1);


		glLightfv(GL_LIGHT2, GL_DIFFUSE, White);
		glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 90);
		glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 1);
		glLightf(GL_LIGHT2, GL_CONSTANT_ATTENUATION, 0);
		glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, 0.0003f);
		glEnable(GL_LIGHT2);

		glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

		glMaterialfv(GL_FRONT, GL_SPECULAR, White);

		//! + Textures | See: https://open.gl/textures		
		ilEnable(IL_ORIGIN_SET);
		ilOriginFunc(IL_ORIGIN_LOWER_LEFT);

		LoadTexture(&grass, L"grass.jpg");
		LoadTexture(&stone, L"stone.tif");
	}

	void NewGame()
	{
		Game.StartGame(5);
		Castle.Reconstruct();
		ResetBall();
		OutputDebugString(L"\nAttempting to start a new game...\n");
		glutPostRedisplay(); // force repaint
	}

	void Update()
	{
		if (Input.Space)
		{
			if (Game.State == GameState::Paused) Game.State = GameState::Playing;
			if (Game.State == GameState::Won || Game.State == GameState::Lost) {
				Game.State = GameState::Ready;
			}
		}

		if (Input.Number > 0)
		{
			switch (Input.Number)
			{
			default:
			case 1:
				Game.Camera = CameraMode::Perspective;
				break;
			case 2:
				Game.Camera = CameraMode::Top;
				break;
			case 3:
				Game.Camera = CameraMode::Ball;
				break;
			}
		}

		if (Game.State != GameState::Playing) return;

		const float angularStep = Input.Left ? -1.0f : Input.Right ? 1.0f : 0.0f;
		for (auto& paddle : Paddles) paddle.Turn(angularStep * TPaddle::StepAngle);

		// increase speed over time
		Ball.Speed = std::clamp(Ball.Speed + 0.0075f * (1.0f/deltaTime), TBall::MinSpeed, TBall::MaxSpeed);
		randomness = std::clamp(randomness + 0.02f * (1.0f/deltaTime), 0.0f, MaxRandomness);

		HandleCollisions();
		Ball.Translate();
	}

	void RenderComponents()
	{
		//! Ground
		glMaterialfv(GL_FRONT, GL_SPECULAR, Green);
		glMaterialf(GL_FRONT, GL_SHININESS, 24.0f);
		glColor3fv(Green);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, grass);
		Ground.Render();
		glDisable(GL_TEXTURE_2D);

		//! Paddles
		glMaterialfv(GL_FRONT, GL_DIFFUSE, Black);
		glMaterialf(GL_FRONT, GL_SHININESS, 0.0f);
		// glColor3fv(Blue);
		for (auto& paddle : Paddles) paddle.Render();


		if (Game.State != GameState::Ready) {

			//! Ball
			glMaterialfv(GL_FRONT, GL_SPECULAR, White);
			glMaterialf(GL_FRONT, GL_SHININESS, 50.0f);
			glColor3fv(Grey);
			//glMaterialfv(GL_FRONT, GL_SPECULAR, White);
			//glMaterialf(GL_FRONT, GL_SHININESS, 50.0f);
			Ball.Render();

			//! Bricks
			glMaterialfv(GL_FRONT, GL_SPECULAR, White);
			glMaterialf(GL_FRONT, GL_SHININESS, 0.0f);
			glColor3fv(White);
			Castle.Render();
		}
	}

#pragma region Include
	// All here because of Linker "redefinition" problems, and similar problems caused by not including *.cpp

	/** Checks if a ball touches a brick. */
	bool IsTouchingBrick(TBall& ball, TWall& castle)
	{
		const auto ballMagnitude = ball.Position.Magnitude();
		const auto ballExtends = std::make_pair(ballMagnitude + ball.Radius, ballMagnitude - ball.Radius);
		if (castle.Radius.first > ballExtends.first || castle.Radius.second < ballExtends.second) return false; // outside area

		//! Check if the brick in this direction is active

		const auto center = TVector::Zeros();
		int brickColumn = 0;
		for (auto& brick : castle.Bricks[0]) {
			if (!brick.IsActive)
			{
				brickColumn++;
				continue;
			}
			
			const auto leftmostPoint = center.Towards(brick.Points[7]); // Leftmost point of the brick.Points
			const auto leftSide = TRay(brick.Points[7], brick.Points[7].Towards(brick.Points[0]));
			const auto rightmostPoint = center.Towards(brick.Points[4]); // Rightmost point of the brick.Points
			const auto rightSide = TRay(brick.Points[4], brick.Points[4].Towards(brick.Points[3]));
			
			const auto fromCenter = center.Towards(ball.Position).Normalized();
			const auto direction = ball.Velocity.Normalized();

			TVector normal;
			if (fromCenter.IsBetween(leftmostPoint, rightmostPoint))
			{				
				normal = fromCenter;				
			}
			else if (direction.IsBetween(ball.Position.Towards(brick.Points[7]),ball.Position.Towards(brick.Points[0])) 
				&& leftSide.Distance(ball.Position) < ball.Radius)
			{
				normal = brick.Points[6].Towards(brick.Points[7]).Normalized();
			}
			else if (direction.IsBetween(ball.Position.Towards(brick.Points[3]),ball.Position.Towards(brick.Points[4]))
				&& rightSide.Distance(ball.Position) < ball.Radius)
			{
				normal = brick.Points[2].Towards(brick.Points[3]).Normalized();
			}
			else
			{
				brickColumn++;
				continue;
			}
			
			//! Break brick
			if (brick.IsSpecial) ball.IsSpecial = true; // Power-up
			castle.Break(brickColumn, ball.IsSpecial);
			randomness = 0;

			//! Bounce
			const float adjust = 1 - randomness * 2.0f; // adjust the direction slightly to make it more 'enjoyable'
			const auto random = Mathf::RandomDirection(-1, 1);
			auto newDirection = ball.Velocity.Reflect(normal).Normalized();
			newDirection = newDirection.Interpolate(random, adjust).Normalized();
			ball.Velocity = newDirection * ball.Speed;
			
			int max = 200;
			const float step = 0.025f;
			const float directionVScenter = direction.Dot(fromCenter.Normalized());
			if (directionVScenter < 0) 
			{
				// Push inside
				while (center.Distance(ball.Position) + Radius > castle.Radius.first && --max > 0) 
				{
					ball.Position += fromCenter * step;
				}
			}
			else
			{
				// Push outside
				while (center.Distance(ball.Position) - Radius < castle.Radius.second && --max > 0) 
				{
					ball.Position += fromCenter * -step;
				}
			}
			return true;
		}
		return false;
	}
#pragma endregion Include

	bool HandleCollisions()
	{
		// x: 1. Paddles (inner, outer radius) | TODO: + sides; edges ?
		// x: 2. Brick (see above)
		// x: 3. Outer bounds

		if (Ball.IsOutOfBounds(Ground.Radius))
		{
			Game.Lives = std::clamp(Game.Lives - 1, 0, Game.Lives);
			if (Game.Lives > 0)
			{
				Game.State = GameState::Paused;
				ResetBall(); // Shift ball in front of the paddle
			}
			else
			{
				Game.EndGame(false);
			}
		}

		else {
			for (auto& paddle : Paddles)
			{
				if (Ball.IsTouchingPaddle(paddle.Radius, paddle.Points, randomness))
				{
					return true;
				}				
			}
			
			if (IsTouchingBrick(Ball, Castle))
			{
				if (Castle.CheckVictory())
				{
					Game.EndGame(true);
				}
				return true;
			}
		}

		return false;
	}



}