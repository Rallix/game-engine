#pragma once

//! OpenGL libraries
#include "GL/freeglut.h"

#include "../Game/Menu.h"

namespace Game::Breakout
{
	/** The main entry point of the application. */
	int Main(int argc, char** argv);

	/** Renders the game. */
	void Render();
	
	/** Reshapes the display when the game window changes. */
	void Reshape(const int newWidth, const int newHeight);
	
	/** Initializes a game of 3D Breakout. */
	void Start();

	/** A periodically called function. */
	void Update();

	const auto deltaTime = 1000 / 60; // 60 FPS	
	/** A timed function, used as a wrapper for Update. */
	inline void Loop(int _)
	{
		Update();		
		glutPostRedisplay();
		glutTimerFunc(deltaTime, Loop, 0);
	}

	// ==============================================	

	/** Renders all game components onscreen. */
	void RenderComponents();

	/** Checks if the ball collided with anything, bounces off, and returs true if it did. */
	bool HandleCollisions();
}