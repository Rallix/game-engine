#include "pch.h"
#include "CppUnitTest.h"

#include "../../Engine/TVector.h"
#include "../../Engine/Mathf.cpp"
#include "../../Engine/TRay.h"
#include "../../Engine/TRay.cpp" //! Including "cpp" solves LNK2019 error


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft::VisualStudio::CppUnitTestFramework
{
	inline std::wstring ToString(const Game::Engine::TVector& vector)
	{
		const auto vecstr = vector.ToString();
		RETURN_WIDE_STRING(vecstr.c_str());
	}
	
	inline std::wstring ToString(const Game::Engine::TRay& ray)
	{
		const auto raystr = ray.ToString();
		RETURN_WIDE_STRING(raystr.c_str());
	}
}

namespace Game::Engine::Tests
{

	TEST_CLASS(RayTests)
	{

	public:

		TRay Ray;
		
		TEST_METHOD_INITIALIZE(InitRay)
		{
			const auto origin = TVector({ 1,0,2 });
			const auto direction = TVector({ 1,8,4 });
			Ray = TRay(origin, direction);
		}

		TEST_METHOD(Distance_RayToRay)
		{
			const auto ray1 = TRay(TVector({0,0,0}), TVector({-1,0,0}));
			const auto ray2 = TRay(TVector{0,0,1}, TVector({ -1,0,0 }));
			const auto expected = 1.0f;
			const auto actual = ray1.Distance(ray2);
			Assert::AreEqual(expected, actual, Mathf::EPSILON, L"The resulting distance doesn't match the expectation.");
		}

		TEST_METHOD(Distance_RayToPoint)
		{
			
			const auto point = TVector(6,0,0);
			const auto ray = TRay(TVector({0,0,0}), TVector({0,-4,-3}));
			const auto expected = 6.0f;
			const auto actual = ray.Distance(point);
			Assert::AreEqual(expected, actual, Mathf::EPSILON, L"The resulting distance doesn't match the expectation.");
		}

		TEST_METHOD(GetPointOnRay)
		{
			const auto ray = TRay(TVector({0,0,0}), TVector({0,-3,4}));
			const auto distance = 5.0f;
			const auto expected = TVector({0,-3,4});
			const auto actual = ray.GetPoint(distance);
			Assert::AreEqual(expected, actual, L"The resulting point doesn't match the expectation.");			
		}
	};
}
