﻿#include "pch.h"
#include "CppUnitTest.h"

#include "../../Engine/TVector.h"
#include "../../Engine/TVector.cpp" //! Including "cpp" solves LNK2019 error

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft::VisualStudio::CppUnitTestFramework
{
	inline std::wstring ToString(const Game::Engine::TVector& vector)
	{
		const auto vecstr = vector.ToString();
		RETURN_WIDE_STRING(vecstr.c_str());
	}
}

namespace Game::Engine::Tests
{

	TEST_CLASS(VectorTests)
	{

	public:

		TEST_METHOD(VectorAnyLength)
		{
			const TVector sizeFour = TVector::Ones(4);
			const TVector sizeSix = TVector({ 0, 0, 0, 0, 1, 1 });
			const TVector sizeThree = TVector::Up();

			Assert::AreEqual(sizeFour.GetLength(), 4,
				L"The size of the created vector doesn't match the expectation.");
			Assert::AreEqual(sizeSix.GetLength(), 6,
				L"The size of the created vector doesn't match the expectation.");
			Assert::AreEqual(sizeThree.GetLength(), 3,
				L"The size of the created vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorTooSmallException)
		{
			auto functor = [] { static_cast<void>(TVector::Ones(1)); };
			Assert::ExpectException<std::invalid_argument>
				(functor, L"1D Vectors are useless, and their creation should be blocked");
		}

		TEST_METHOD(VectorIterate)
		{
			TVector begin = TVector::Ones(6);
			for (auto& c : begin)
			{
				c -= 1;
			}
			const TVector expected = TVector::Zeros(6);

			Assert::AreEqual(expected, begin, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorAddition)
		{
			const TVector one = TVector(1, -1, 0);
			const TVector two = TVector(2, 2, -2);

			const TVector actual = one + two;
			const TVector expected = TVector(3, 1, -2);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorAdditionWrongSizeException)
		{
			const TVector one = TVector::Ones(2);
			const TVector two = TVector({ 1, 2, 3, 4, 5 });

			auto functor = [&] { static_cast<void>(one + two); };
			Assert::ExpectException<std::invalid_argument>
				(functor, L"It shouldn't be possible to add two vectors of different sizes.");
		}

		TEST_METHOD(VectorScalarMultiplication)
		{
			const TVector one = TVector(1, -4, 2);
			const float value = -2.0f;

			const TVector actual = one * value;
			const TVector expected = TVector(-2, 8, -4);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorElementWiseMultiplication)
		{
			const TVector one = TVector(2, -3, 6);
			const TVector two = TVector(7, 3, 2);

			const TVector actual = one * two;
			const TVector expected = TVector(14, -9, 12);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorMultiplicationWrongSizeException)
		{
			const TVector one = TVector::Ones(12);
			const TVector two = TVector({ 1, 2, 3, 4, 5 });

			auto functor = [&] { static_cast<void>(one * two); };
			Assert::ExpectException<std::invalid_argument>
				(functor, L"It shouldn't be possible to multiply two vectors of different sizes.");
		}

		TEST_METHOD(VectorSubtraction)
		{
			const TVector one = TVector(4, 2, -3);
			const TVector two = TVector(6, -1, -4);

			const TVector actual = one - two;
			const TVector expected = TVector(-2, 3, 1);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorDotProduct)
		{
			const TVector one = TVector(-6, 8);
			const TVector two = TVector(5, 12);

			const float actual = one.Dot(two);
			const float expected = 66;

			Assert::AreEqual(expected, actual, L"The resulting value doesn't match the expectation.");
		}

		TEST_METHOD(Vector3DCrossProduct)
		{
			const TVector one = TVector({ 2, 3, 4 });
			const TVector two = TVector({ 5, 6, 7 });

			const TVector actual = one.Cross(two);
			const TVector expected = TVector(-3, 6, -3);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(Vector2DCrossProduct)
		{
			const TVector one = TVector({ 2, 3 }); // -> {2, 3, 0}
			const TVector two = TVector({ 5, 6 }); // -> {5, 6, 0}

			const TVector actual = one.Cross(two);
			const TVector expected = TVector(0, 0, -3);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(Vector4DCrossProductException)
		{
			const TVector one = TVector({ 2, 3, 4, 5 });
			const TVector two = TVector({ 5, 6, 7, 8 });

			auto functor = [&] { static_cast<void>(one.Cross(two)); };
			Assert::ExpectException<std::invalid_argument>
				(functor,
					L"Cross product of vectors of large dimensions isn't defined and shouldn't be allowed.");
		}

		TEST_METHOD(VectorCrossProductWrongSizeException)
		{
			const TVector one = TVector({ 2, 3, 4 });
			const TVector two = TVector({ 5, 6 });

			auto functor = [&] { static_cast<void>(one.Cross(two)); };
			Assert::ExpectException<std::invalid_argument>
				(functor, L"Cross product of vectors of different length shouldn't be allowed.");
		}

		TEST_METHOD(VectorAngle)
		{
			const TVector one = TVector(-12, 16);
			const TVector two = TVector(12, 9);

			const float actual = one.Angle(two);
			const float expected = 90;

			Assert::AreEqual(expected, actual, Mathf::EPSILON,
				L"The resulting value doesn't match the expectation.");
		}

		TEST_METHOD(VectorMagnitude)
		{
			const TVector one = TVector(4, 3);

			const float actual = one.Magnitude();
			const float expected = 5;

			Assert::AreEqual(expected, actual, Mathf::EPSILON,
				L"The resulting value doesn't match the expectation.");
		}

		TEST_METHOD(VectorNormalize)
		{
			const TVector one = TVector(4, 0, 0);

			const float expected = 1;
			const float actual = one.Normalized().Magnitude();

			Assert::AreEqual(expected, actual, Mathf::EPSILON,
				L"The resulting value doesn't match the expectation.");
		}

		TEST_METHOD(VectorInvert)
		{
			const TVector one = TVector(4, 0, 0);

			const TVector expected = TVector(-4, 0, 0);
			const TVector actual = one.Inverted();

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorTowards)
		{
			const TVector start = TVector(3, 3);
			const TVector target = TVector(4, 1);

			const TVector expected = TVector(1, -2); // target - start
			const TVector actual = start.Towards(target);

			Assert::AreEqual(expected, actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(VectorDistance)
		{
			const TVector start = TVector::Zeros(4);
			const TVector target = TVector({ 0, 0, 0, 16 });


			const float expected = 16; // (target - start).length
			const float actual = start.Distance(target);

			Assert::AreEqual(expected, actual, Mathf::EPSILON,
				L"The resulting vector doesn't match the expectation.");
		}
	};
}
