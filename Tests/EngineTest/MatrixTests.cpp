#include "pch.h"
#include "CppUnitTest.h"

#include "../../Engine/TVector.h"
#include "../../Engine/TMatrix.h"
#include "../../Engine/TMatrix.cpp" //! Including "cpp" solves LNK2019 error

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Microsoft::VisualStudio::CppUnitTestFramework
{
	inline std::wstring ToString(const Game::Engine::TVector& vector)
	{
		const auto vecstr = vector.ToString();
		RETURN_WIDE_STRING(vecstr.c_str());
	}

	inline std::wstring ToString(const Game::Engine::TMatrix& matrix)
	{
		const auto vecstr = matrix.ToString();
		RETURN_WIDE_STRING(vecstr.c_str());
	}

}

namespace Game::Engine::Tests
{

	TEST_CLASS(MatrixTests)
	{
	public:

		TMatrix Matrix;

		TEST_METHOD_INITIALIZE(InitMatrix)
		{
			Matrix = TMatrix({ {1, 0, 2}, {2, 3, 5}, {1, 8, 4} });
		}

		TEST_METHOD(TestToNeedlesslyPadLength)
		{
			Assert::IsTrue(true, L"The universe is probably imploding.");
		}

		TEST_METHOD(IterateMatrix)
		{
			float sum1 = 0;
			const float expectedSum1 = 0 + 3 + 8;
			for (const auto& v : Matrix)
			{
				sum1 += v[1];
			}
			Assert::AreEqual(expectedSum1, sum1, L"The iterator didn't go through all the rows.");
		}

		TEST_METHOD(CreateSquare)
		{
			Assert::IsTrue(Matrix.IsSquare(), L"The created matrix isn't square which isn't supported.");

			Assert::AreEqual(Matrix.GetRow(0), TVector(1, 0, 2),
				L"The first row of the matrix doesn't match the expectations.");
			Assert::AreEqual(Matrix.GetRow(1), TVector(2, 3, 5),
				L"The second row of the matrix doesn't match the expectations.");
			Assert::AreEqual(Matrix.GetRow(2), TVector(1, 8, 4),
				L"The third row of the matrix doesn't match the expectations.");
		}

		TEST_METHOD(CreateNon3x3)
		{
			auto three = TVector({ 1,2,3 });
			auto five = TVector({ 1,2,3,4,5 });

			auto createMatrix = [three, five]() { auto error = TMatrix({ three, five, three }); };
			Assert::ExpectException<std::invalid_argument>(createMatrix);
		}

		TEST_METHOD(EqualMatrices)
		{
			const auto matrixEq = TMatrix({ {1, 0, 2}, {2, 3, 5}, {1, 8, 4} });

			Assert::IsTrue(Matrix == matrixEq, L"The matrices should be equal, but they aren't.");
		}

		TEST_METHOD(NonEqualMatrices)
		{
			const auto matrixNeq = TMatrix({ {1, 0, 2}, {2, -3, 5}, {1, 8, 4} });

			Assert::IsTrue(Matrix != matrixNeq, L"The matrices should not be equal, but they are.");
		}

		TEST_METHOD(AddMatrices)
		{
			const auto matrix = TMatrix({ {3, 1, 0}, {2, 7, -2}, {-6, 3, 0} });

			const auto actual = Matrix + matrix;
			const auto expected = TMatrix({ {4, 1, 2}, {4, 10, 3}, {-5, 11, 4} });

			Assert::IsTrue(expected == actual, L"The resulting matrix doesn't match the expectation.");
		}

		TEST_METHOD(MultiplyMatrixByScalar)
		{
			const auto scalar = -2.0f;

			const auto actual = Matrix * scalar;
			const auto expected = TMatrix({ {-2, 0, -4}, {-4, -6, -10}, {-2, -16, -8} });

			Assert::IsTrue(expected == actual, L"The resulting matrix doesn't match the expectation.");
		}

		TEST_METHOD(MultiplyMatrixByVector)
		{
			const auto matrix = TMatrix({ {1, 2, 3}, {4, 5, 6}, {7, 8, 9} });
			const auto vector = TVector(2,1, 3);

			const auto actual = matrix * vector;
			const auto expected = TVector(13, 31, 49);

			Assert::IsTrue(expected == actual, L"The resulting vector doesn't match the expectation.");
		}

		TEST_METHOD(MultiplyMatrixByMatrix)
		{
			const auto matrix = TMatrix({ {3, 1, 6}, {-4, 2, 8}, {1, 8, 4} });

			const auto actual = Matrix * matrix;
			const auto expected = TMatrix({ {5, 17, 14}, {-1, 48, 56}, {-25, 49, 86} });

			Assert::IsTrue(expected == actual, L"The resulting matrix doesn't match the expectation.");
		}

		TEST_METHOD(TransposeMatrix)
		{
			const auto actual = Matrix.Transposed();
			const auto expected = TMatrix({ {1, 2, 1}, {0, 3, 8}, {2, 5, 4} });

			Assert::IsTrue(expected == actual, L"The resulting matrix doesn't match the expectation.");
		}

		TEST_METHOD(DeterminantMatrix)
		{
			const auto actual = Matrix.Determinant();
			const auto expected = -2;

			Assert::IsTrue(expected == actual, L"The resulting matrix doesn't match the expectation.");
		}

		TEST_METHOD(InverseMatrix)
		{
			const auto matrix = TMatrix({ {3, 0, 2}, {2, 0, -2}, {0, 1, 1} });
			const auto actual = matrix.Inverted();
			const auto expected = TMatrix({ {0.2f, 0.2f, 0.0f}, {-0.2f, 0.3f, 1.0f}, {0.2f, -0.3f, 0.0f} });

			Assert::IsTrue(expected == actual, L"The resulting matrix doesn't match the expectation.");
		}
	};
}
