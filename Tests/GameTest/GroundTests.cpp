﻿#include "pch.h"
#include "CppUnitTest.h"

#include "../../Game/TGround.h"
#include "../../Game/TGround.cpp"

namespace Game::Breakout::Tests
{
	using namespace Microsoft::VisualStudio::CppUnitTestFramework;
	using namespace Game::Breakout;
	
	TEST_CLASS(GroundTests)
	{
	public:

		
		// TGround Ground = TGround(16.0f);	
		
		TEST_METHOD(CirclePointPlacement)
		{
			const size_t points = 4;
			const TGround ground8(1, points);
			Assert::IsTrue(ground8.Points.size() == points + 1, L"The number of points in the circle doesn't match the expectations.");

			auto expectedPoints = vector<TVector>({TVector(1,0,0), TVector(0,0,1), TVector(-1,0,0), TVector(0,0,-1), TVector(1,0,0)});
			for (size_t p = 0; p < expectedPoints.size(); p++) {
				Assert::IsTrue(ground8.Points[p] == expectedPoints[p], L"One of the circle points doesn't match the expectation.");
			}
		}
	};
}
